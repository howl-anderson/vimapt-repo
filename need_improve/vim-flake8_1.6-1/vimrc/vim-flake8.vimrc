" Depends: this script depends on flake8 package in pip


" If you don't want to use the <F7> key for flake8-checking,
" simply remap it to another key.
" It autodetects whether it has been remapped and won't register
" the <F7> key if so. For example, to remap it to <F3> instead, use:
"
" autocmd FileType python map <buffer> <F3> :call Flake8()<CR>


" For flake8 configuration options please consult the following page:
"
" https://flake8.readthedocs.org/en/latest/config.html


" To customize the location of your flake8 binary, set g:flake8_cmd:
"
" let g:flake8_cmd="/opt/strangebin/flake8000"


" To customize the location of quick fix window, set g:flake8_quickfix_location:
"
" let g:flake8_quickfix_location="topleft"


" To customize the height of quick fix window, set g:flake8_quickfix_height:
"
" let g:flake8_quickfix_height=7


" To customize whether the quickfix window opens, set g:flake8_show_quickfix:
"
" let g:flake8_show_quickfix=0  " don't show
" let g:flake8_show_quickfix=1  " show (default)


" To customize whether the show signs in the gutter, set g:flake8_show_in_gutter:
"
" let g:flake8_show_in_gutter=0  " don't show (default)
" let g:flake8_show_in_gutter=1  " show


" To customize whether the show marks in the file, set g:flake8_show_in_file:
"
" let g:flake8_show_in_file=0  " don't show (default)
" let g:flake8_show_in_file=1  " show


" To customize the number of marks to show, set g:flake8_max_markers:
"
" let g:flake8_max_markers=500  " (default)


" To customize the gutter markers, set any of flake8_error_marker,
" flake8_warning_marker, flake8_pyflake_marker, flake8_complexity_marker,
" flake8_naming_marker. Setting one to the empty string disables it. Ex.:
"
" flake8_error_marker='EE'     " set error marker to 'EE'
" flake8_warning_marker='WW'   " set warning marker to 'WW'
" flake8_pyflake_marker=''     " disable PyFlakes warnings
" flake8_complexity_marker=''  " disable McCabe complexity warnings
" flake8_naming_marker=''      " disable naming warnings


" To customize the colors used for markers, define the highlight groups,
" Flake8_Error, Flake8_Warning, Flake8_PyFlake, Flake8_Complexity, Flake8_Naming:
"
" " to use colors defined in the colorscheme
" highlight link Flake8_Error      Error
" highlight link Flake8_Warning    WarningMsg
" highlight link Flake8_Complexity WarningMsg
" highlight link Flake8_Naming     WarningMsg
" highlight link Flake8_PyFlake    WarningMsg


" Tips
"
" A tip might be to run the Flake8 check every time you write a Python file,
" to enable this, add the following line to your .vimrc file (thanks Godefroid!):
"
" autocmd BufWritePost *.py call Flake8()



" This plugin goes well together with the following plugin:
"
" PyUnit (unit test helper under <F8> and <F9>)
