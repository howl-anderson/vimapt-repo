from vimapt.hook import base_hook


class JediHook(base_hook.BaseHook):
    def pre_install(self):
        self.install_pip_package('jedi')
        return True

    def post_install(self):
        return True

    def pre_remove(self):
        return True

    def post_remove(self):
        return True

_VIMAPT_HOOK_CLASS=JediHook