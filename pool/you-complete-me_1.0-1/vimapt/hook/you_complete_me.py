import os
import subprocess

from vimapt.hook import base_hook


class YouCompleteMeHook(base_hook.BaseHook):
    def pre_install(self):
        process = subprocess.Popen(['python', os.path.join(self.package_path, 'install.py')], stdout=subprocess.PIPE)
        output, _ = process.communicate()
        print(output)
        return True

    def post_install(self):
        return True

    def pre_remove(self):
        return True

    def post_remove(self):
        return True

_VIMAPT_HOOK_CLASS=YouCompleteMeHook