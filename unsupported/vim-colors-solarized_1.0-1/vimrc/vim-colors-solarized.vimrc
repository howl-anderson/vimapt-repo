syntax enable
set background=dark
colorscheme solarized

let g:solarized_termcolors=256
call togglebg#map("<F5>")
